const libraryName = 'Dinamik';

const webpack = require('webpack'),
    path = require('path');

module.exports = function (env) {
    return {
        context: path.resolve(__dirname, '../src'),
        entry: ['./main.js'],
        output: {
            path: path.resolve(__dirname, '../dist'),
            publicPath: '/',
            filename: libraryName + (env.production ? '.min' : '.[hash]') + '.js',
            sourceMapFilename: libraryName + (env.production ? '.min' : '.[hash]') + '.js.map',
            library: libraryName,
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        resolve: {
            extensions: ['.js', '.ts'],
            modules: [path.resolve(__dirname, '../src'), path.resolve(__dirname, '../node_modules')]
        },
        module: {
            rules: [{
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            }]
        },
        plugins: [
            new webpack.EnvironmentPlugin({
                NODE_ENV: env.development ? 'development' : (env.production ? 'production' : (env.test ? 'test' : 'development')),
            })
        ]
    };
}